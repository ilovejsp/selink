<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="net.selink.dao.SelinkDAO"%>
<%@page import="net.selink.dto.SelinkDTO"%>

<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>시크릿 링크</title>
<link rel="stylesheet" href="css/style_table.css" type="text/css" media="screen"/>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!--theme-style-->
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />	
<link rel="stylesheet" href="css/etalage.css" type="text/css" media="all" />
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--fonts-->
<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'>
<!--//fonts-->
<script src="js/jquery.min.js"></script>
<script src="js/jquery.blockUI.js"></script>
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/easing.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="js/jquery.contentcarousel.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script>
function bigImg(x) {
    x.style.height = "500px";
    x.style.width = "500px";
}

function normalImg(x) {
    x.style.height = "150px";
    x.style.width = "150px";
}
</script>
    <style>
        *{
            margin:0;
            padding:0;
        }
        body{
            font-family: Georgia, serif;
            font-size: 20px;
            font-style: italic;
            font-weight: normal;
            letter-spacing: normal;
            background: #f0f0f0;
        }
        #content{
            background-color:#fff;
            width:750px;
            padding:40px;
            margin:0 auto;
            border-left:30px solid #1D81B6;
            border-right:1px solid #ddd;
            -moz-box-shadow:0px 0px 16px #aaa;
        }
        .head{
            font-family:Helvetica,Arial,Verdana;
            text-transform:uppercase;
            font-weight:bold;
            font-size:12px;
            font-style:normal;
            letter-spacing:3px;
            color:#888;
            border-bottom:3px solid #f0f0f0;
            padding-bottom:10px;
            margin-bottom:10px;
        }
        .head a{
            color:#1D81B6;
            text-decoration:none;
            float:right;
            text-shadow:1px 1px 1px #888;
        }
        .head a:hover{
            color:#f0f0f0;
        }
        #content h1{
            font-family:"Trebuchet MS",sans-serif;
            color:#1D81B6;
            font-weight:normal;
            font-style:normal;
            font-size:56px;
            text-shadow:1px 1px 1px #aaa;
        }
        #content h2{
            font-family:"Trebuchet MS",sans-serif;
            font-size:34px;
            font-style:normal;
            background-color:#f0f0f0;
            margin:40px 0px 30px -40px;
            padding:0px 40px;
            clear:both;
            float:left;
            width:100%;
            color:#aaa;
            text-shadow:1px 1px 1px #fff;
        }

    </style>
<script type="text/javascript">
$(document).ready(function(){
    $("#block").click(function(){
        $.blockUI({
           message: $('#loginForm')
        });
        
        setTimeout($.unblockUI,20000);
        
        
        
    });
    $('#cancel').click(function() { 
        $.unblockUI(); 
        return false; 
    }); 
    
    
});
</script>
</head>
<body> 
<!--header-->
	<div class="header">
		<div class="container">
			<div class="logo">
				<a href="selink.s"><span class="logo-h"><span></a>
				<h1>시크릿<span>링크</span></h1>
			</div>
		</div>
		<label class="line"> </label>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="top-nav">
				<span class="menu"> </span>
				<ul>
					<li><a href="selink.s">시크릿링크</a></li>
					<li><a href="category.s">전체카테고리</a></li>
					<li><a href="" id="block">링크 추천</a></li>
				</ul>
				<!-- script-nav -->
			<script>
			$("span.menu").click(function(){
				$(".top-nav ul").slideToggle(500, function(){
				});
			});
			</script>
				<script type="text/javascript">
					jQuery(document).ready(function($) {
						$(".scroll").click(function(event){		
							event.preventDefault();
							$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
						});
					});
					</script>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!--//header-->
<!--content-->
	<div class="content" align="center">
					<table align="center">
						<tr>
	
							<td><a href="selink_category1.s"><img  border="0" src="./etc_image/p2p.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/p2p_1.jpg';" onmouseout="this.src='./etc_image/p2p.jpg';"></a></td>	
							<td><a href="selink_category2.s"><img border="0" src="./etc_image/cafe.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/cafe_1.jpg';" onmouseout="this.src='./etc_image/cafe.jpg';"></a></td>	
						</tr>
						<tr>
							<!--
								<img >
							-->
							<td><a href="selink_category3.s"><img border="0" src="./etc_image/blog.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/blog_1.jpg';" onmouseout="this.src='./etc_image/blog.jpg';"></a></td>	
							<td><a href="selink_category4.s"><img border="0" src="./etc_image/lecture.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/lecture_1.jpg';" onmouseout="this.src='./etc_image/lecture.jpg';"></a></td>	
						</tr>
						<tr>
							<td><a href="selink_category5.s"><img border="0" src="./etc_image/shop.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/shop_1.jpg';" onmouseout="this.src='./etc_image/shop.jpg';"></a></td>	
							<td><a href="selink_category6.s"><img border="0" src="./etc_image/game.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/game_1.jpg';" onmouseout="this.src='./etc_image/game.jpg';"></a></td>	
						</tr>
						<tr>
							<td><a href="selink_category7.s"><img border="0" src="./etc_image/etc.jpg" alt="Smiley" width="150" height="150" onmouseover="this.src='./etc_image/etc_1.jpg';" onmouseout="this.src='./etc_image/etc.jpg';"></a></td>	
						</tr>
						
					</table>
		</div>
<!--content-->
	<div class="content" align="center">
            <h1>파일공유</h1>
            <table class="table1">
                <thead>
                    <tr>
                        <th scope="col" abbr="Medium">순서</th>
                        <th scope="col" abbr="Medium">제목</th>
                        <th scope="col" abbr="Business">내용</th>
                        <th scope="col" abbr="Deluxe">링크</th>
                        <th scope="col" abbr="Deluxe">추천수</th>
                        <th scope="col" abbr="Deluxe">글쓴이</th>
                        <th scope="col" abbr="Deluxe">날짜</th>
                        <th scope="col" abbr="Deluxe">아이피</th>
 						<th scope="col" abbr="Deluxe">추천</th>                       
                    </tr>
                </thead>
<%
SelinkDAO dao = new SelinkDAO();
//SelinkDTO dto=dao.selectTopBoard();
List<SelinkDTO> list = new ArrayList();
list=dao.selectCategory1();

for(int i=0; i<list.size(); i++){
	System.out.println(list.get(i).getSelink_list_category_title());
%> 

                <tbody>
                    <tr>
                        <td><%=list.get(i).getSelink_list_no()%></td>
                        <td><%=list.get(i).getSelink_list_title()%></td>
                        <td><%=list.get(i).getSelink_list_content()%></td>
                        <td><a href="<%=list.get(i).getSelink_list_link()%>" target="_blank">클릭</a></td>
                        <td><%=list.get(i).getSelink_list_crazy()%></td>
                        <td><%=list.get(i).getSelink_list_writer()%></td>
                        <td><%=list.get(i).getSelink_list_date()%></td>
                        <td><%=list.get(i).getSelink_list_ip()%></td>
                        <td><a href='crazy.s?no=<%=list.get(i).getSelink_list_no()%>'>추천</a></td>

                    </tr>
                    </tbody>
                    <%
}
%>	
                    
            </table>


        </div>
<!-- loginform -->
<div id="loginForm" style="display: none" align="center">
<form method="get" action="write_ok.s">
		<table>
			<tr>
				<td>제목</td><td><input type="text" id="title" name="title" maxlength="10" size="10" required></td>
			</tr>
			<tr>
				<td>내용</td><td><input type="text" id="contents" name="contents" maxlength="15" size="10" required></td>
			</tr>
			<tr>
				<td>링크</td><td><input type="text" id="link" name="link" maxlength="50" size="10" required></td>
			</tr>
			<tr>
				<td>글쓴이</td><td><input type="text" id="writer" name="writer" maxlength="10" size="10" required></td>
			</tr>
			<tr>
				<td>카테고리</td>
				<td><select id="category" name="category">
					<option value="기타">기타</option>
					<option value="파일공유">파일공유</option>
					<option value="블로그">블로그</option>
					<option value="카페">카페</option>
					<option value="쇼핑">쇼핑</option>
					<option value="게임">게임</option>
					<option value="강의">강의</option>
				</select></td>
			</tr>
			<tr>
				<td><input type="submit" value="글쓰기"></td>
				<td><input type="button" value="취소" id="cancel"></td>
			</tr>
		</table>
	</form>
</div>
		
<!--footer-->
	<div class="footer">
		<div class="container">
			 <p class="footer-class">개발자:okwow123@naver.com &copy; 2014 Template by <a href="http://w3layouts.com/" target="_blank">W3layouts</a> </p>
			 <div class="clearfix"> </div>
		</div>
		 <script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
	</div>
</body>
</html>