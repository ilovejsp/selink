package net.selink.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.selink.dao.SelinkDAO;



@SuppressWarnings("serial")
public class SelinkController extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uri=request.getRequestURI();
		String command=uri.substring(uri.lastIndexOf("/")+1,uri.lastIndexOf(".s"));

		if(command !=null && command.trim().equals("selink")){
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
		else if(command !=null && command.trim().equals("category")){
			RequestDispatcher rd = getServletContext().getRequestDispatcher("/category.jsp");
			rd.forward(request, response);
		}
		else if(command !=null && command.trim().equals("crazy")){
			response.setContentType("text/html; charset=utf-8");
			PrintWriter out = response.getWriter();
			boolean crazy_flag=true;
			
			Cookie[] cookies = request.getCookies();
			for(int i=0; i<cookies.length; i++){
				if(cookies[i].getName().equals("crazy")){
					crazy_flag=false;
				}
			}
			
			if(crazy_flag==false){
				out.println("<script>");
				out.println("alert('추천은 10분에 한번만 가능합니다');");
				out.println("history.back();");
				out.println("</script>");							
			}else{
				Cookie cookie = new Cookie("crazy","crazy");
				cookie.setMaxAge(60*10);
				cookie.setPath("/");
				response.addCookie(cookie);
				
				int selinkListNo=Integer.parseInt(request.getParameter("no"));
				SelinkDAO dao= new SelinkDAO();
				dao.updateCrazy(selinkListNo);
				out.println("<script>");
				out.println("alert('추천됬습니다.!!!');");
				out.println("history.back();");
				out.println("</script>");
			}
		

		}
		else if(command !=null && command.matches("selink_category.*")){
			if(command.trim().equals("selink_category1")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category1.jsp");
				rd.forward(request, response);
			}
			else if(command.trim().equals("selink_category2")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category2.jsp");
				rd.forward(request, response);
			}
			else if(command.trim().equals("selink_category3")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category3.jsp");
				rd.forward(request, response);
			}
			else if(command.trim().equals("selink_category4")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category4.jsp");
				rd.forward(request, response);
			}
			else if(command.trim().equals("selink_category5")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category5.jsp");
				rd.forward(request, response);
			}
			else if(command.trim().equals("selink_category6")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category6.jsp");
				rd.forward(request, response);
			}
			else if(command.trim().equals("selink_category7")){
				RequestDispatcher rd = getServletContext().getRequestDispatcher("/category7.jsp");
				rd.forward(request, response);
			}
		}
		else if(command !=null && command.trim().equals("write_ok")){
			response.setContentType("text/html; charset=utf-8");
			PrintWriter out = response.getWriter();
			boolean write_flag=true;
			
			Cookie[] cookies = request.getCookies();
			for(int i=0; i<cookies.length; i++){
				if(cookies[i].getName().equals("write")){
					write_flag=false;
				}
			}
			
			if(write_flag==false){
				out.println("<script>");
				out.println("alert('글쓰기는 10분에 한번만 가능합니다');");
				out.println("history.back();");
				out.println("</script>");							
			}else{
				Cookie cookie = new Cookie("write","write");
				cookie.setMaxAge(60*10);
				cookie.setPath("/");
				response.addCookie(cookie);
				
				request.setCharacterEncoding("utf-8");
				SelinkDAO dao=new SelinkDAO();
				String title=request.getParameter("title");
				title = new String(title.getBytes("8859_1"), "UTF-8");
				String ip;
				String contents=request.getParameter("contents");
				contents = new String(contents.getBytes("8859_1"), "UTF-8");

				if (request.getHeader("HTTP_X_FORWARDED_FOR") == null) {
				    ip= request.getRemoteAddr();
				} else {
				    ip= request.getHeader("HTTP_X_FORWARDED_FOR");
				}
				String link=request.getParameter("link");
				link = new String(link.getBytes("8859_1"), "UTF-8");
				if(!link.contains("http://")){
					link="http://"+link;
				}
				String writer=request.getParameter("writer");
				writer = new String(writer.getBytes("8859_1"), "UTF-8");

				String category=request.getParameter("category");
				category = new String(category.getBytes("8859_1"), "UTF-8");

				boolean flag=false;
				flag=dao.insertWrite(title, contents, link, category, writer, ip);
				if(flag==true){
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/category.jsp");
					rd.forward(request, response);
				}
				else{
					RequestDispatcher rd = getServletContext().getRequestDispatcher("/wrong.html");
					rd.forward(request, response);
				}
			}
			
		}
		
		else
			response.sendRedirect("wrong.html");
			
		
	}
	
	

}
