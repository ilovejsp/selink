package net.selink.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import net.selink.dto.SelinkDTO;


public class SelinkDAO {
	private static Connection conn;

	public SelinkDAO(){
		
		try{
			Class.forName("com.mysql.jdbc.Driver");
		}catch(ClassNotFoundException ex){
			System.out.println("no driver");
		}
		
		try{
			conn = DriverManager.getConnection("jdbc:mysql://localhost:3307/selink","root","root");
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
			
		}
	}

	public boolean insertWrite(String title,String contents,String link,String category,String writer,String ip){
		int result=0;
		boolean return_flag=false;
		String query="insert into selink_list(selink_list_ip,selink_list_category_title,selink_list_title,selink_list_content,"
				+ "selink_list_link,selink_list_date,selink_list_crazy,selink_list_writer) values(?,?,?,?,?,now(),?,?)";
		try{
			PreparedStatement pstmt=conn.prepareStatement(query);
			pstmt.setString(1, ip);
			pstmt.setString(2, category);
			pstmt.setString(3, title);
			pstmt.setString(4, contents);
			pstmt.setString(5, link);
			pstmt.setInt(6, 0);
			pstmt.setString(7, writer);
			result=pstmt.executeUpdate();
			
			if(result==1){
				return_flag=true;
			}
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return return_flag;
	}
	
	public boolean updateCrazy(int selink_list_no){
		int result=0;
		boolean return_flag=false;
		String query="update selink_list set selink_list_crazy=selink_list_crazy+1 where selink_list_no=?";
		try{
			PreparedStatement pstmt=conn.prepareStatement(query);
			pstmt.setInt(1, selink_list_no);
			result=pstmt.executeUpdate();
			
			if(result==1){
				return_flag=true;
			}
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return return_flag;
	}
	
		
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectTopBoard(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_crazy,selink_list_writer,selink_list_date,selink_list_ip,selink_list_no from selink_list order by selink_list_crazy desc limit 10";
		
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = new SelinkDTO();
		try{
			Statement stmt=conn.createStatement();
			ResultSet rs=stmt.executeQuery(query);
			while(rs.next()){
				dto.setSelink_list_category_title(rs.getString(1));
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_crazy(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));

				dto.setSelink_list_no(rs.getInt(9));
				
				list.add(dto);
			}
			rs.close();
			stmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectAllBoard(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));

				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	//파일공유
	public List<SelinkDTO> selectCategory1(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='파일공유' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));
				
				dto.setSelink_list_crazy(rs.getInt(9));
				
				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	//카페
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectCategory2(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='카페' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));

				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	//블로그
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectCategory3(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='블로그' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));

				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	//강의
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectCategory4(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='강의' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));
				
				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	//쇼핑
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectCategory5(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='쇼핑' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));
				
				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	//게임
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectCategory6(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='게임' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));
				
				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}

	//etc
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<SelinkDTO> selectCategory7(){
		String query="select selink_list_category_title,selink_list_title,selink_list_content,selink_list_link,selink_list_no,selink_list_writer,selink_list_date,selink_list_ip,selink_list_crazy from selink_list where selink_list_category_title='기타' order by selink_list_no desc";
		List<SelinkDTO> list = new ArrayList();
		SelinkDTO dto = null;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				dto=new SelinkDTO();
				dto.setSelink_list_category_title(rs.getString(1));
				
				dto.setSelink_list_title(rs.getString(2));
				
				dto.setSelink_list_content(rs.getString(3));
				
				dto.setSelink_list_link(rs.getString(4));

				dto.setSelink_list_no(rs.getInt(5));

				dto.setSelink_list_writer(rs.getString(6));

				dto.setSelink_list_date(rs.getString(7));
			
				dto.setSelink_list_ip(rs.getString(8));
				
				dto.setSelink_list_crazy(rs.getInt(9));

				list.add(dto);
				
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return list;
	}
	
	//ip
	public void insertIp(String ip){
		String query="insert into selink_ip(selink_ip,selink_ip_date) values(?,now())";
		try{
			PreparedStatement pstmt=conn.prepareStatement(query);
			pstmt.setString(1, ip);
			pstmt.executeUpdate();
			
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
	}
	//ip
	public static int selectIpCount(){
		String query="select count(distinct selink_ip) from selink_ip";
		int result=0;
		try{
			ResultSet rs= null;
			PreparedStatement pstmt=conn.prepareStatement(query);
			rs=pstmt.executeQuery();
			while(rs.next()){
				result=rs.getInt(1);
			}
			rs.close();
			pstmt.close();
			SelinkDAO.close();
		}catch(SQLException se){
			System.out.println(se.getLocalizedMessage());
		}
		return result;
	}


	public static void close(){
		try{
			if(conn!= null){
				conn.close();
				conn=null;
			}
		}catch(SQLException se ){
			System.out.println("close error");
		}
	}
	
}
