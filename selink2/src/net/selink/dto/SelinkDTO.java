package net.selink.dto;

public class SelinkDTO {
	private int selink_list_no;//순서
	private String selink_list_ip;//아이피
	private String selink_list_category_title;//분류
	private String selink_list_title;//제목
	private String selink_list_content;//내용
	private String selink_list_link;//링크
	private String selink_list_date;//날짜
	private int selink_list_crazy;//추천수
	private String selink_list_writer;//글쓴이
	private String selink_list_image;//이미지
	public int getSelink_list_no() {
		return selink_list_no;
	}
	public void setSelink_list_no(int selink_list_no) {
		this.selink_list_no = selink_list_no;
	}
	public String getSelink_list_ip() {
		return selink_list_ip;
	}
	public void setSelink_list_ip(String selink_list_ip) {
		this.selink_list_ip = selink_list_ip;
	}
	public String getSelink_list_category_title() {
		return selink_list_category_title;
	}
	public void setSelink_list_category_title(String selink_list_category_title) {
		this.selink_list_category_title = selink_list_category_title;
	}
	public String getSelink_list_title() {
		return selink_list_title;
	}
	public void setSelink_list_title(String selink_list_title) {
		this.selink_list_title = selink_list_title;
	}
	public String getSelink_list_content() {
		return selink_list_content;
	}
	public void setSelink_list_content(String selink_list_content) {
		this.selink_list_content = selink_list_content;
	}
	public String getSelink_list_link() {
		return selink_list_link;
	}
	public void setSelink_list_link(String selink_list_link) {
		this.selink_list_link = selink_list_link;
	}
	public String getSelink_list_date() {
		return selink_list_date;
	}
	public void setSelink_list_date(String selink_list_date) {
		this.selink_list_date = selink_list_date;
	}
	public int getSelink_list_crazy() {
		return selink_list_crazy;
	}
	public void setSelink_list_crazy(int selink_list_crazy) {
		this.selink_list_crazy = selink_list_crazy;
	}
	public String getSelink_list_writer() {
		return selink_list_writer;
	}
	public void setSelink_list_writer(String selink_list_writer) {
		this.selink_list_writer = selink_list_writer;
	}
	public String getSelink_list_image() {
		return selink_list_image;
	}
	public void setSelink_list_image(String selink_list_image) {
		this.selink_list_image = selink_list_image;
	}
	

}
	
